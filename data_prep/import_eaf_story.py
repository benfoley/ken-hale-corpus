# Use it for single file conversion like this:
# python3 import_eaf_story.py -i filename.eaf -o filename.json

import argparse
import json
import re
import os
from lxml import etree
from pympi.Elan import Eaf


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile",  type=str, default="test.eaf", help="The input file to convert.")
    parser.add_argument("-o", "--outfile", type=str, help="Filename to save. Defaults to use same basename as input")
    parser.add_argument("-b", "--indir",   type=str, help="Dir to bulk process files from. Defaults to cwd")
    parser.add_argument("-d", "--outdir",  type=str, help="Dir to save files into. Defaults to cwd")
    parser.add_argument('-t', '--tier',    type=str, default='Phrase', help='Target language tier name')
    parser.add_argument('-g', '--glosstier', type=str, default='Free Translation', help='Gloss tier name')
    args = parser.parse_args()

    outdir = args.outdir or os.getcwd()
    tier = args.tier
    gloss_tier = args.glosstier

    if args.indir is not None:
        print('bulk docs')
        for entry in os.scandir(args.indir):
            if not entry.name.startswith('.') and entry.is_file():
                print(entry.name)
                process_file(os.path.join(args.indir, entry.name), None, outdir)
    else:
        if os.path.isfile(args.infile):
            process_file(args.infile, args.outfile, outdir, tier, gloss_tier)
        else:
            print('no file at', args.infile)


def process_file(infile, outfile, outdir, tier, gloss_tier):

    title = os.path.splitext(os.path.basename(infile))[0]
    outfile = outfile or (title + '-eaf-story.json').lower()

    input_eaf = Eaf(infile)
    # Get annotations and params (things like speaker id) on the target tier
    annotations = sorted(input_eaf.get_annotation_data_for_tier(tier))
    params = input_eaf.get_parameters_for_tier(tier)
    print(params)

    phrases_data = []

    for id, ann in enumerate(annotations):
        start = ann[0]
        end = ann[1]
        vernacular = ann[2]

        phrase_meta = {'id': id+1, 'begin_time_offset': start, 'end_time_offset': end}

        # This method gets ref annotations that have same start OR end time
        # cheat to only get start overlaps by offsetting the comparison time
        gloss = input_eaf.get_ref_annotation_at_time('Free Translation', start+1)
        # print(gloss)

        try:
            gloss = gloss[0][2]
        except IndexError:
            gloss = ''

        print("")
        print(start, end)
        print("A:", vernacular)
        print("G:", gloss)

        phrases_data.append({"meta": phrase_meta, "words": {'vernacular': vernacular, "gloss": gloss}})

    # Compile the phrases
    json_data = {'title': title, 'img': '', 'intro': '', 'phrases': phrases_data}

    with open(os.path.join(outdir, outfile), 'w') as outfile_file:
        json.dump(json_data, outfile_file, ensure_ascii=False, indent=4)

    print(json.dumps(json_data, indent=4, sort_keys=True))
    print('- done')

main()
