# Use it for single file conversion like this:
# python3 import_csv_story.py -i filename.csv -o filename.json
# python3 data_prep/import_csv_story.py -i data_prep/mokeefe.csv -o data/stories/mokeefe.json

"""

Expected CSV Format:
start time,end time,Spoken text,English translation
0'00.0044,0'13.0435,AIATSIS archive recording. Harold Koch fieldtape 78/9-7 side A from the Koch 803 collection.,
0'13.0435,0'15.0865,"\k HK: Ye, alkaperte.",
0'15.0865,0'17.7314,\k Alkaperte artntwenke? (Yeah),
0'17.7314,0'26.0290,"\k Wele aynanthe pweleke akwenherre, wantewarlepe, apmerarle Emu Bore warlepe .","\e Well, we had put the cattle into what's it—into the Emu Bore paddock."
0'26.0290,0'29.5562,\k Kwerepenhartepe aynanthe ngwetyanpe aperinenyerre.,\e Then we took them out in the morning.
0'29.5562,0'35.9220,\k Nhape aynanthe arrtyeyenenherre apmerele perlaynelepe.,\e Then we went and held them in a clear place.
0'35.9220,0'38.7354,\k Nhape aynantherre awepareyayne kwereye.,\e Then we waited for him [the helicopter man] for some time.
"""

import argparse
import json
import os
import csv
import sys
import re

def main():
    parser = argparse.ArgumentParser(description="Convert CSV files to structured JSON format.")
    parser.add_argument(
        "-i", "--infile", type=str, 
        help="The input CSV file to convert."
    )
    parser.add_argument(
        "-o", "--outfile", type=str, 
        help="Filename to save the JSON output. Defaults to use the same basename as input with .json extension."
    )
    parser.add_argument(
        "-b", "--indir", type=str, 
        help="Directory to bulk process CSV files from. Defaults to cwd if not specified."
    )
    parser.add_argument(
        "-d", "--outdir", type=str, 
        help="Directory to save JSON files into. Defaults to cwd."
    )
    
    args = parser.parse_args()

    outdir = args.outdir or os.getcwd()

    if args.indir:
        # Bulk processing
        if not os.path.isdir(args.indir):
            print(f"Input directory '{args.indir}' does not exist or is not a directory.", file=sys.stderr)
            sys.exit(1)
        print("Bulk processing CSV files...")
        for entry in os.scandir(args.indir):
            if entry.is_file() and entry.name.lower().endswith('.csv') and not entry.name.startswith('.'):
                infile_path = os.path.join(args.indir, entry.name)
                outfile_name = (os.path.splitext(entry.name)[0] + '.json').lower()
                outfile_path = os.path.join(outdir, outfile_name) if args.outdir else outfile_name
                print(f"Processing '{entry.name}' -> '{outfile_name}'")
                process_file(infile_path, outfile_path)
    elif args.infile:
        # Single file processing
        if not os.path.isfile(args.infile):
            print(f"Input file '{args.infile}' does not exist.", file=sys.stderr)
            sys.exit(1)
        outfile = args.outfile or (os.path.splitext(os.path.basename(args.infile))[0] + '.json').lower()
        outfile_path = os.path.join(outdir, outfile) if args.outdir else outfile
        print(f"Processing '{args.infile}' -> '{outfile}'")
        process_file(args.infile, outfile_path)
    else:
        print("No input file or directory specified. Use -i for a single file or -b for bulk processing.", file=sys.stderr)
        sys.exit(1)

import sys

def parse_time(timestamp):
    """
    Converts timestamp from format m'ss.mmmmmm (or higher precision) to integer milliseconds.
    Example: "0'13.043512" -> 13043 ms
    """
    timestamp = timestamp.strip()
    if not timestamp:
        print(f"Empty timestamp encountered.", file=sys.stderr)
        return None
    
    parts = timestamp.split("'")
    if len(parts) != 2:
        print(f"Invalid timestamp format: '{timestamp}'. Expected format m'ss.mmmmmm", file=sys.stderr)
        return None
    
    try:
        minutes = int(parts[0])
        seconds_fraction = float(parts[1])  # Float can handle the extra precision
    except ValueError:
        print(f"Error parsing timestamp: '{timestamp}'. Non-numeric values found.", file=sys.stderr)
        return None

    # Multiply seconds_fraction by 1000 to get total milliseconds, rounded to the nearest integer
    total_milliseconds = minutes * 60 * 1000 + round(seconds_fraction * 1000)
    return total_milliseconds

def process_file(infile, outfile):
    title = os.path.splitext(os.path.basename(infile))[0]
    phrases_data = []
    
    try:
        with open(infile, 'r', encoding='utf-8-sig') as csvfile:
            reader = csv.DictReader(csvfile)
            expected_fields = ['start time', 'end time', 'Spoken text', 'English translation']
            if reader.fieldnames != expected_fields:
                print(f"Warning: Expected CSV headers {expected_fields}, but got {reader.fieldnames}. Proceeding with available headers.", file=sys.stderr)
            
            for idx, row in enumerate(reader, start=1):
                start = row.get('start time', '').strip()
                end = row.get('end time', '').strip()
                vernacular = row.get('Spoken text', '').strip()
                gloss = row.get('English translation', '').strip()
                
                # Debugging: Print the current row's start and end times
                print(f"Row {idx}: start='{start}', end='{end}'")
                
                # Skip rows with empty start or end times
                if not start or not end:
                    print(f"Warning: Row {idx} has empty 'start time' or 'end time'. Skipping this row.", file=sys.stderr)
                    print(f"Row content: {row}", file=sys.stderr)
                    continue
                
                # Remove leading '\e' and '\k' using regex
                vernacular = re.sub(r'^\\[ek]\s*', '', vernacular)
                vernacular = re.sub(r'/', '', vernacular)
                gloss = re.sub(r'^\\[ek]\s*', '', gloss)
                gloss = re.sub(r'/', '', gloss)
                
                # Clean up surrounding quotes if present
                vernacular = vernacular.strip('"') if vernacular.startswith('"') and vernacular.endswith('"') else vernacular
                gloss = gloss.strip('"') if gloss.startswith('"') and gloss.endswith('"') else gloss
                
                # Convert timestamps to integer milliseconds
                begin_time_offset = parse_time(start)
                end_time_offset = parse_time(end)
                
                if begin_time_offset is None or end_time_offset is None:
                    print(f"Warning: Row {idx} has invalid timestamps. Skipping this row.", file=sys.stderr)
                    print(f"Row content: {row}", file=sys.stderr)
                    continue
                
                phrase_meta = {
                    'id': idx,
                    'begin_time_offset': begin_time_offset,
                    'end_time_offset': end_time_offset
                }
                
                words = {
                    'vernacular': vernacular,
                    'gloss': gloss
                }
                
                phrases_data.append({
                    'meta': phrase_meta,
                    'words': words
                })
    except Exception as e:
        print(f"Error processing file '{infile}': {e}", file=sys.stderr)
        return

    json_data = {
        'title': title,
        'img': '',
        'intro': '',
        'phrases': phrases_data
    }

    try:
        # Ensure the output directory exists
        os.makedirs(os.path.dirname(outfile), exist_ok=True)
        with open(outfile, 'w', encoding='utf-8') as jsonfile:
            json.dump(json_data, jsonfile, ensure_ascii=False, indent=4)
        print(f"Successfully wrote JSON to '{outfile}'")
    except Exception as e:
        print(f"Error writing JSON to '{outfile}': {e}", file=sys.stderr)

if __name__ == "__main__":
    main()