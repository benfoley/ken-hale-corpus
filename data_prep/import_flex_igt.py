# Use it for single file conversion like this:
# python3 import_flex_igt.py -i filename.flextext -o filename.json

# Bulk conversion by specifying the bulk dir:
# python3 import_flex_igt.py -b data -d ../data/kenhale/

import argparse
import json
import os
from lxml import etree


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile",  type=str, default="data/kh4556.flextext", help="The input file to convert.")
    parser.add_argument("-o", "--outfile", type=str, help="Filename to save. Defaults to use same basename as input")
    parser.add_argument("-b", "--indir",   type=str, help="Dir to bulk process files from. Defaults to cwd")
    parser.add_argument("-d", "--outdir", type=str, help="Dir to save files into. Defaults to cwd")
    args = parser.parse_args()

    outdir = args.outdir or os.getcwd()

    if args.indir is not None:
        print('bulk docs')
        for entry in os.scandir(args.indir):
            if not entry.name.startswith('.') and entry.is_file():
                print(entry.name)
                process_file(os.path.join(args.indir, entry.name), None, outdir)
    else:
        if os.path.isfile(args.infile):
            process_file(args.infile, args.outfile, outdir)
        else:
            print('no file at', args.infile)


def process_file(infile, outfile, outdir):

    title = os.path.splitext(os.path.basename(infile))[0]
    outfile = outfile or (title + '.json').lower()

    print(outdir, outfile)

    print('Working on', title)

    tree = etree.parse(infile)
    root = tree.getroot()

    # Process XML for phrase data
    phrases_data = []
    for phrase in root.iter('phrase'):

        phrase_meta = {}
        phrase_trans = ''
        publish = True

        # Build info about the phrase: id, translation, speaker, lang
        for item in phrase.findall('item'):

            # *PUB
            if item.attrib['type'] == 'note':
                if item.text and '*PUB' in item.text:
                    publish = False
            phrase_meta['publish'] = publish

            # print('item', item.attrib, item.text, publish)

            # Phrase ID
            if item.attrib['type'] == 'segnum':
                phrase_meta['id'] = item.text

            # Phrase translation aka gloss
            if item.attrib['type'] == 'gls' and item.text:
                phrase_trans = item.text

            # Look for speaker and lang data << replace these tokens with real names!
            if item.attrib['type'] == 'note':
                if item.text in ['KH', 'LP', 'LW', 'TJ']:
                    data = {'KH': 'Ken Hale', 'LP': 'Louis Penangke', 'LW': 'Lindsay Woods', 'TJ': 'Long Tommy Jampijinpa'}
                    speaker = '{'+item.text+'}'
                    phrase_meta['speaker'] = speaker.format(**data)
                elif item.text in ['EN', 'K', 'MU', 'WA']:
                    data = {'EN': 'English', 'K': 'Kaytetye', 'MU': 'Mudburra', 'WA': 'Western Arrernte'}
                    lang = '{'+item.text+'}'
                    phrase_meta['lang'] = lang.format(**data)

                # Look for image data
                if item.text and 'jpg' in item.text:
                    phrase_meta['img'] = item.text

        if 'guid' in phrase.attrib:
            phrase_meta['guid'] = phrase.attrib['guid']

        # Change hyphens to underscore for compatibility with Go templates
        if 'begin-time-offset' in phrase.attrib:
            phrase_meta['begin_time_offset'] = phrase.attrib['begin-time-offset']
        else:
            print('missing begin-time-offset for', phrase_meta['guid'])

        if 'end-time-offset' in phrase.attrib:
            phrase_meta['end_time_offset'] = phrase.attrib['end-time-offset']
        else:
            print('missing end-time-offset for', phrase_meta['guid'])

        # Compile words. Work out spacing for punctuation by iterating reverse
        phrase_words = []

        # Loop through all the words in this phrase
        for word in phrase.find('words').iterchildren():
            word_vernacular = None
            word_gloss = None
            space_after = True

            # Word vernacular and gloss
            # findall gets direct children
            for item in word.findall('item'):

                if item.attrib['type'] in ['txt', 'punct']:
                    word_vernacular = item.text

                if item.attrib['type'] == 'gls':
                    word_gloss = item.text

                if item.text in ['(', '[']:
                    space_after = False

                # work out the spacing
                if word.getnext() is not None:
                    next_item = word.getnext().find('item')
                    if next_item.text in ['.', ',', '?', '!', '"', "'", ')', ']']:
                        space_after = False

                # print(word_vernacular, word_gloss, space_after)

            # Morphemes
            # iter gets all descendant tags
            morpheme_data = []
            for morpheme in word.iter('morph'):
                morph = {}
                for morph_item in morpheme.iter('item'):
                    if morph_item.attrib['type'] == 'txt' and morph_item.text is not '':
                        morph['txt'] = morph_item.text
                    if morph_item.attrib['type'] == 'gls' and morph_item.text is not '':
                        morph['gls'] = morph_item.text
                if 'gls' in morph:
                    morpheme_data.append(morph)

            # Compile the word and morpheme data into a word object
            # and append to phrase words array
            phrase_words.append({'vernacular':  word_vernacular,
                                 'gloss': word_gloss,
                                 'morphemes': morpheme_data,
                                 'space': space_after})

        # Compile the words
        if publish:
            phrases_data.append({'meta': phrase_meta, 'words': phrase_words, 'gloss': phrase_trans})
        else:
            phrases_data.append({'meta': phrase_meta})

    # Compile the phrases
    json_data = {'title': title, 'phrases': phrases_data}

    # Write out json data
    with open(os.path.join(outdir, outfile), 'w') as outfile_file:
        json.dump(json_data, outfile_file, ensure_ascii=False, indent=4)

    print('- done')


main()
