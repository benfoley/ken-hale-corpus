# Use it for single file conversion like this:
# python3 import_trs_story.py -i filename.trs -o filename.json

import argparse
import json
import re
import os
from lxml import etree


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile",  type=str, default="test.trs", help="The input file to convert.")
    parser.add_argument("-o", "--outfile", type=str, help="Filename to save. Defaults to use same basename as input")
    parser.add_argument("-b", "--indir",   type=str, help="Dir to bulk process files from. Defaults to cwd")
    parser.add_argument("-d", "--outdir", type=str, help="Dir to save files into. Defaults to cwd")
    args = parser.parse_args()

    outdir = args.outdir or os.getcwd()

    if args.indir is not None:
        print('bulk docs')
        for entry in os.scandir(args.indir):
            if not entry.name.startswith('.') and entry.is_file():
                print(entry.name)
                process_file(os.path.join(args.indir, entry.name), None, outdir)
    else:
        if os.path.isfile(args.infile):
            process_file(args.infile, args.outfile, outdir)
        else:
            print('no file at', args.infile)


def process_file(infile, outfile, outdir):

    title = os.path.splitext(os.path.basename(infile))[0]
    outfile = outfile or (title + '-trs-story.json').lower()

    tree = etree.parse(infile)
    root = tree.getroot()

    # Process XML for phrase data
    phrases_data = []
    regex = re.compile(r'[\n\r\t]')

    # Cheating:
    # assuming single Episode, single Section, single Turn
    # because that's what the sample data haa!

    for id, el in enumerate(root.iter('Sync')):
        tail = regex.sub(' ', el.tail).lstrip()

        # Phrase start time. Other meta?
        s_time = el.attrib['time']
        phrase_meta = {'id': id+1, 'begin_time_offset': 1000 * float(s_time)}

        # Get the end time
        if el.getnext() is not None:
            e_time = el.getnext().get('time')
            phrase_meta['end_time_offset'] = 1000 * float(e_time)
        else:
            e_time = root.find('.//Episode/Section/Turn').get('endTime')
            phrase_meta['end_time_offset'] = 1000 * float(e_time)

        # Include backslash codes here to split upon
        # Also, catch lines that haven't been lang-tagged
        # with start of line followed by newline
        parts = re.split(r'(^\n|\\k|\\e|\\si|\\st)', tail)

        if parts[0] is '':
            parts.pop(0)

        vernacular = []
        gloss = []
        for i in range(len(parts)):
            # Some tiers are not encoded with lang
            if i == 0 and parts[i] not in ['\k', '\e', '\si', '\st']:
                vernacular.append(parts[i].strip())
            # Otherwise, copy text into vernacular or gloss according to the lang code
            if parts[i] == '\k':
                vernacular.append(parts[i+1].strip())
            if parts[i] == '\e':
                gloss.append(parts[i+1].strip())

        phrases_data.append({"meta": phrase_meta, "words": {'vernaculars': vernacular, "glosses": gloss}})

    # Compile the phrases
    json_data = {'title': title, 'img': '', 'intro': '', 'phrases': phrases_data}

    # Write out json data
    with open(os.path.join(outdir, outfile), 'w') as outfile_file:
        json.dump(json_data, outfile_file, ensure_ascii=False, indent=4)

    print(json.dumps(json_data, indent=4, sort_keys=True))
    print('- done')


main()
