---
title: "{{ replace .TranslationBaseName "-" " " | lower }}"
date: {{ .Date }}
body_classes: "transcript"
type: "stories"
---
