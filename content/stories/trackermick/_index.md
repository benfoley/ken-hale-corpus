---
title: "Tracker Mick Kapetye"
date: 2018-01-24T17:25:38+10:00
body_classes: "transcript stories"
type: "stories"
---

Tracker Mick Kapetye was from Etwerrpe Country and spent much of his life working on cattle stations. In his later years he lived at Alekarenge, where he did a lot of work with linguist Harold Koch. In this story Tracker Mick recalls a droving expedition in which he and Don Ross were part of, probably in the 1940s. They took cattle from Murray Downs station to Alice Springs. Mick told most of the story, getting confirmation on things from Don as he talked. At the time of this muster, Alf Morris was the manager at Murray Downs station.

© Intellectual content Tracker Mick 1986

Reproduced with permission from family members Jacob Peltharr and Hilda Price 2002.

© Recording Emily Hayes and CAAMA. Used with Permission.
