---
title: "Daisy Kemarre"
date: 2018-01-20T15:38:57+10:00
body_classes: "transcript stories"
type: "stories"
image: "daisy.jpg"
image_credit: "Daisy Kemarre 2003 Photo: Myfany Turpin"
---

Daisy Kemarre told this story to Emily Hayes at Alekarenge in 1986 for the Kaytetye show on CAAMA radio. Daisy and Emily are ‘sisters’, as Daisy's mother and Emily's mother were Penangke women from the estate Warlekerlange, west of Barrow Creek. Daisy passed away in May 2010.

In this story Daisy Kemarre from Arnerre tells of the first missionaries at Barrow Creek, shepherding goats, travelling with her family in the bush and teaching ceremony; and of how she met her husband, Alec Kapetye a Kaytetye man from Kwerrkepentye. Alec was a stockman who worked at Neutral Junction station.

Transcribed and translated by Myfany Turpin, Alison Ross, Shirleen McLaughlin, Emily Hayes.

© Intellectual content Daisy Kemarre
© Recording Emily Hayes and CAAMA. Reproduced with permission of Daisy’s family members: Alison Ross, Amy Ngamperle, Hilda Ngamperle and Lena Ngamperle.

Other stories by Daisy Kemarre can be read in Kaytetye Country by Harold Grace and Grace Koch.
