---
title: "Bob Kemarre /Ampetyane"
date: 2018-01-22T17:17:05+10:00
---

Bob Kemarre /Ampetyane grew up at Barrow Creek. Ken Hale notes his bush name as Rlwamparwengenye. His traditional lands are Arnerre, a Rain Dreaming country to the north of Barrow Creek through his father, whose name is noted as Arlkalkenhe.  Kaytetye people recall his mother was called Akartnenge and that she was from Ertwerrpe. Bob Kemarr is the only speaker to narrate a story in this collection.  In his story he talks about his brothers Mick (*Alwengarenye*) and Norman (*Akarlpanke*) and his wife, Annie or *Itenty-ampek* from an Alyawarr Fire Dreaming country, and his first child. Descendants of his extended family live in the Barrow Creek region; it is not known whether he has any direct descendants, as Kaytetye speakers believe he moved to his wife’s Country.

### Listen to Bob Kemarre

- [kh4565](/stories/kh4565)
