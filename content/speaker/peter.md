---
title: "Peter"
date: 2018-01-22T17:17:05+10:00
---

Peter. It is not clear who this person was.  On some recordings Ken Hale can be heard eliciting in Warlpiri (e.g. 4560), with Peter translating into Kaytetye. Many of Peter’s words lack the rounding sound heard in many Kaytetye words today (Rounding is written with ‘w’). For example he says *arrpantye* instead of *arrpwantye*; *amparrenke* instead of *ampwarrenke* ‘die’; *mpelarre* instead of *mpwelarre* ‘rainbow’; *tererre* instead of *twererre* ‘clapsticks’ This suggests that the Peter may have been a native speaker of  Western Arrernte rather than Kaytetye.

### Listen to Peter

- [kh4560](/kenhale/kh4560)
- [kh4561](/kenhale/kh4561)
- [kh4562](/kenhale/kh4562)
- [kh4563](/kenhale/kh4563)
