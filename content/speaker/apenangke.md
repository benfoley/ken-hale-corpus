---
title: "Aileen Penangke"
date: 2018-01-22T17:16:26+10:00
---

Aileen Penangke told this story to Emily Hayes on 20 May 1985 for for the Kaytetye show on CAAMA radio. Aileen Penangke was probably born around 1925. Her father's country is Warlekerlange, from Barrow Creek extending westwards. In this story she recalls travelling with Ruby Kngwarraye, from Etwerrpe, whose bush name is Ilpakethe. Aileen refered to Ruby as 'nyanye' (mother's mother) or 'atyerreye' (younger sibling). She also refers to travelling with Alec Kapetye’s mother and their dog called Entyelkayte. The events described in the story probably occured just prior to WWII. Aileen passed away ca 1990 when she was swept away in the flood water crossing Tara creek. Ruby passed away in 1999.

Transcribed and translated by Myfany Turpin, Emily Hayes, Joanie Ross, Alison Ross & Shirleen McLaughlin.

© Intellectual content Aileen Penangke

Reproduced with permission from family members Patsy Ngampeyarte, Tommy Thompson and Alison Ross 2002 and 2017.

© Recording Emily Hayes and CAAMA. Used with Permission.

### Listen to Aileen Penangke

- [apenangke](/stories/apenangke)
