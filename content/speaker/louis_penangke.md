---
title: "Louis Penangke"
date: 2018-01-22T17:16:26+10:00
---

Louie Penangke was from Apmarnepentye, an Anmatyerr region just to the south of Kaytetye Country. Louis, whose bush name was Kaljirbuka, or Alkanye, grew up at Wauchope and worked at the Wauchope wolfram mine in the 1930s and at Stirling Station in 1938. He was possibly working on a station near Elliott when Ken Hale came through in 1959. Ken Hale first speaks an English phrase and then Long Tommy says the equivalent in Mudburra. This is followed by Louis saying the equivalent in Kaytetye. On this recording Ken Hale can be heard repeating both the Mudburra and Kaytetye sentences.  Louie went on to act as a guide for TGH Strehlow in 1968 through Anmatyerr country and sang Anmatyerr songs for him, as did his father, Paddy Pengarte, 30 years earlier.

### Listen to Louis Penangke

- [kh4556](/kenhale/kh4556)
