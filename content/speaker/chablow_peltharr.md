---
title: "Chablow Peltharr"
date: 2018-01-22T17:17:05+10:00
---

Chablow Peltharr (Kapetye) grew up at Neutral Junction Station. He worked for the pastoral industry at Murry Downs, Singleton and Neutral Junction Stations and was a much sought after stockman in the region (Ross & Whitebeach 2007). His traditional lands are Jarra-jarra, a kangaroo Dreaming country north-west of Barrow Creek. His mother is from Tywekwatye of Karwe-Karlwe, Devils Marbles. Chablow must have spoken Warlpiri, as we know Ken Hale used Warlpiri as a guide for this elicitation (4563, 4564); although no Warlpiri can be heard on the recordings. Chablow must have also spoken Alyawarr, as he is joined by Nelson (possibly Nelson Pwerle from Jarra-Jarra) who translates Chablow’s Kayteye into Alyawarr on 4566. Chablow’s son Billy Bumper lives at Murry Downs with his extended family.  

### Listen to Chablow Peltharr

- [kh4563](/kenhale/kh4563)
- [kh4564](/kenhale/kh4564)
- [kh4566](/kenhale/kh4566)
