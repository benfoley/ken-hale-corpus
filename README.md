Watch out for file name case! Our files should be lowercase.

Put WAV and FLEX/EAF files into data_prep folder

From the project folder, rename to lowercase (this script is for OSX)

    for f in data_prep/*; do mv "$f" "$f.tmp"; mv "$f.tmp" "`echo $f | tr "[:upper:]" "[:lower:]"`"; done


Convert wav to mp3 and output into `static/audio/`

    ffmpeg -i data_prep/caama1010.wav -vn -ar 44100 -ac 1 -ab 64k -f mp3 static/audio/caama1010.mp3



Convert .flextext file to .json:

    python3 data_prep/xml_to_json.py --infile data_prep/caama1010.eaf >> data_prep/caama1010.json

Replace characters in the json (these are from xml process converting attribute names to keys):
Doing this in Sublime Text for the moment....

replace:
^(\s+")[@#]

with:
$1

For Flex files, also replace `interlinear-text` in the json with `interlinear`. Hugo can't handle hyphens in property names.

Put the json file into `data/kenhale/` or `data/stories`

    mv data_prep/caama1010.json data/stories/caama1010.json;


Make a new content page, kenhale or stories,

    hugo new stories/caama1010.md;


Move the md file into a subfolder. Doing this means we use a `list.html` template to show the content, as Hugo looks for this template first for folder indexes.

    mkdir content/stories/caama1010;
    mv content/stories/caama1010.md content/stories/caama1010/_index.md


Put images into /content/stories/caama1010/images/

Add the page to `index.md` content.

---

Using
https://github.com/mkay581/scroll-js

Javascript for
- audio playback is in `layouts/partials/audio-script.html`
- toggle tiers is in `layouts/partials/kenhale-footer.html`

---

The site is built with Hugo, and deployed to Gitlab sites when you do git push.

```
brew install hugo
hugo server
```

## Dev notes

Clone it
```bash
git clone git@gitlab.com:benfoley/ken-hale-corpus.git
```

CD
```bash
cd  ken-hale-corpus
```

Install theme
```bash
git submodule update --init --recursive
```

Start it
```bash
hugo server
```

Deploy (make changes, git commit). Auto deploy on push
```bash
git push
```
